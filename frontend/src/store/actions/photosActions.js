import photosSlice from "../slices/photosSlice";

export const {
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess,
  deletePhotosRequest,
  deletePhotosFailure,
  deletePhotosSuccess,
  postPhotosRequest,
  postPhotosFailure,
  postPhotosSuccess,
  fetchUserPhotosRequest,
  fetchUserPhotosFailure,
  fetchUserPhotosSuccess,
} = photosSlice.actions;
