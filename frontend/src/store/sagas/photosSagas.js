import axiosApi from "../../axiosApi";
import { put, takeEvery } from "redux-saga/effects";
import { addNotification } from "../actions/notifierActions";
import { historyPush } from "../actions/historyActions";

import {
  deletePhotosFailure,
  deletePhotosRequest,
  deletePhotosSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess,
  fetchUserPhotosFailure,
  fetchUserPhotosRequest,
  fetchUserPhotosSuccess,
  postPhotosFailure,
  postPhotosRequest,
  postPhotosSuccess,
} from "../actions/photosActions";

export function* fetchPhotos({ payload: userId }) {
  try {
    let url = "/photos";

    if (userId) {
      url += "?user=" + userId;
    }

    const response = yield axiosApi.get(url);
    yield put(fetchPhotosSuccess(response.data));
  } catch (e) {
    yield put(fetchPhotosFailure());
    yield put(addNotification({ message: "Fetch photos failed", options: { variant: "error" } }));
  }
}

export function* deletePhotos(action) {
  try {
    yield axiosApi.delete("/photos/" + action.payload.id);
    yield put(deletePhotosSuccess());
    yield put(addNotification({ message: "Your Photo Deleted", options: { variant: "success" } }));
    yield put(fetchPhotosRequest(action.payload.idGallery));
  } catch (e) {
    yield put(deletePhotosFailure());
    yield put(addNotification({ message: "Delete photos failed", options: { variant: "error" } }));
  }
}

export function* postPhotos({ payload: photoData }) {
  try {
    yield axiosApi.post("/photos", photoData);
    yield put(postPhotosSuccess());
    yield put(
      addNotification({ message: "Your Photo Successfully Added ", options: { variant: "success" } })
    );
    yield put(historyPush("/"));
  } catch (e) {
    yield put(postPhotosFailure());
    yield put(addNotification({ message: "Your Photo has not been added", options: { variant: "error" } }));
  }
}

export function* fetchUserPhotos({ payload: userId }) {
  try {
    const response = yield axiosApi.get("/users?user=" + userId);
    yield put(fetchUserPhotosSuccess(response.data));
  } catch (e) {
    yield put(fetchUserPhotosFailure());
  }
}

const photosSagas = [
  takeEvery(fetchPhotosRequest, fetchPhotos),
  takeEvery(deletePhotosRequest, deletePhotos),
  takeEvery(postPhotosRequest, postPhotos),
  takeEvery(fetchUserPhotosRequest, fetchUserPhotos),
];

export default photosSagas;
