import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddNewPhoto from "./containers/AddNewPhoto/AddNewPhoto";
import AllPhoto from "./containers/AllPhoto/AllPhoto";
import UserGallery from "./containers/UserGallery/UserGallery";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

const App = () => {
  const user = useSelector((state) => state.users.user);

  return (
    <Layout>
      <Switch>
        <Route path="/" exact component={AllPhoto} />
        <Route path="/user/:id" component={UserGallery} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <ProtectedRoute path="/add-photo" component={AddNewPhoto} isAllowed={user} redirectTo="/login" />
      </Switch>
    </Layout>
  );
};

export default App;
